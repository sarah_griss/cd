package at.sarah.griss.cds;

public class Cd {
	private String name;

	public Cd(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
