package at.sarah.griss.cds;

import java.util.List;

public class Performer {
	private String firstName;
	private String lastName;

	private List<Cd> cds;
	private List<Dvd> dvds;

	public Performer(String firstName, String lastName, List<Cd> cds, List<Dvd> dvds) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.cds = cds;
		this.dvds = dvds;
	}

	public void addCD(Cd cd) {
		this.cds.add(cd);
	}

	public void adDvd(Dvd dvd) {
		this.dvds.add(dvd);
	}

}
