package at.sarah.griss.cds;

public interface Playable {
	void play();
	void stop();
	void pause();

}
